%% Materials Properties List
%   Comprehensive List of all relevant Materials and Properties
%   Also Radiative Heat Flux of Sun(Sunny & Cloudy Conditions)
%     obj = Material(name,k, c_p, rho,T_vap,E_vap)
%         name         % name used for comparison
%         k            % thermal conductivity: W/mK
%         c_p          % specific heat capacity: J/kgK
%         rho          % density: kg/m^3
%         T_vap        % (optional)vaporization temperature: C
%         E_vap        % (optional) enthalpy of vaporizaton: J
%         alpha_s      % (optional)radiation absorptivity: W/m2
%         T_melt       % (optional) melting temperature: C
%         cost         % (optional) cost: dollars/kg

% Radiative Heat Flux of Sun: W/m^2
sunny = 1000; % No Cloud Cover
cloudy = 752; % Mild Cloud Cover
vcloudy = 270; % Severe Cloud Cover
blackhole = 0; % RIP

% Water
water = Material('water',0.598,4182,997,100,2453.5e3);
water.alpha_s = 0.2;

% Air
air = Material('air',25.87e-3,1.005e3,1.225);

% Floaty Materials

aluminum = Material('aluminum',205,921.1,2700);
aluminum.T_melt = 660;
aluminum.cost = 1.49;

steel = Material('steel',16.2,500,8000);
steel.T_melt = 1370;
steel.cost = 2.56;

nichrome = Material('nichrome',11.3,450,8400);
nichrome.T_melt = 1400;
nichrome.cost = 8.00;

tungsten = Material('tungsten',178,132.51,19250);
tungsten.T_melt = 3422;
tungsten.cost = 100;

brass = Material('brass',109,920,8587);
brass.T_melt = 900;
brass.cost = 2.16;

magnesium = Material('magnesium',156,1050,1740);
magnesium.T_melt = 650;
magnesium.cost = 2.45;

carbide = Material('carbide',360,750,3100);
carbide.T_melt = 2730;
carbide.cost = 36.00;

styrofoam = Material('styrofoam',0.033,1300,1060);
styrofoam.T_melt = 240;
styrofoam.cost = 0.11;

% Toob Materials
plex = Material('plex',0.17,1.46e-3,1180);
plex.T_melt = 200;
plex.cost = 2.47;

polycarb = Material('polycarb',0.19,1200,1200);
polycarb.T_melt = 155;
polycarb.cost = 4.90;

pyrex = Material('pyrex',1.143,750,2230);
pyrex.T_melt = 820;
pyrex.cost = 1.82;

pdms = Material('pdms',0.15,1.46,0.97);
pdms.T_melt = -40;
pdms.cost = 437;

glass = Material('glass',.8,.84e3,2500);
glass.T_melt = 1400;
glass.cost = 1.35;
