%% Edge object
%   Represents connection between two nodes

classdef Edge < handle
    properties
        nodes   % 2x1 vector of nodes connected by edge
    end

    methods
        function obj = Edge(nodes)
            if(nargin <1)
                %% empty edge
                %%error('Edge must have nodes to connect defined');
            else
                obj.nodes = nodes;
            end
        end

        function ind = includes(obj,node)
            % function returning index or not a node is referenced by the edge
            % returns 0 if not found
            ind  = find(obj.nodes == node);
            if(isempty(ind)); ind = 0; end
        end
    end
end
