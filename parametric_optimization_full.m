% script to run parametric optimization of simulation

% current inputs: 
%   float material
%   tube material
%   radii of tube
%   radii of float

sim_properties
in_to_m = .0254;
% example run
inputs = struct('float_mat',aluminum,'tube_mat',plex,'R_tube',[1 .75]*in_to_m,'R_float',[.5 0]*in_to_m);

% float_mats = [aluminum, steel, nichrome, tungsten, brass, magnesium, carbide, styrofoam,glass];
float_mats = [aluminum, steel, nichrome, tungsten, brass, magnesium, carbide, glass];
tube_mats = [plex, polycarb, pyrex, glass];

R_tubes = [1,      .75;
1.315,  1;
1.6,    1.25;
1.9,    1.5;
2.375,  2;
2.875,  2.5;
3.5,    3;]*in_to_m;

% output value storage in column vectors matching sweep
sweep_N = 3*size(R_tubes,1)*numel(float_mats)*numel(tube_mats); % 3 accounts for 3 different float radii per tube radius
steam_rate = zeros(1,sweep_N); 
sweep_index = 1;
float_temp_max = steam_rate; 
tube_temp_max = steam_rate;
cost = steam_rate;
safety_factor = steam_rate;
key = struct();

for(R_tube=R_tubes') % loop tube radii
    disp(['setting tube radius to ' num2str(R_tube(1)) ', ' num2str(R_tube(2))]);
    inputs.R_tube = R_tube';
    for(r=1:2:5) % loop float radii
        R_float = [.7/r*R_tube(2),0];
        disp(['setting float radius to ' num2str(R_float(1))]);
        inputs.R_float = R_float;
        for(tube_mat=tube_mats) % loop tube materials
                disp(['setting tube material to ' tube_mat.name;]);
                inputs.tube_mat = tube_mat;
            for(float_mat=float_mats) % loop float materials
                tic;
                % set parameter
                disp(['setting float material to ' float_mat.name;]);
                inputs.float_mat = float_mat;
%         keyboard

                % run simulation
                disp('running simulation');
                outputs = sim_parameterization(inputs);
                key(sweep_index).R_tube = inputs.R_tube;
                key(sweep_index).R_tube_scale = .7/r;
                key(sweep_index).R_float = inputs.R_float;
                key(sweep_index).float_mat = inputs.float_mat;
                key(sweep_index).tube_mat = inputs.tube_mat;

                disp('storing outputs');
                % store outputs
                steam_rate(sweep_index) = outputs.steam_rate;
                float_temp_max(sweep_index) = outputs.float_temp_max;
                tube_temp_max(sweep_index) = outputs.tube_temp_max;
                sweep_index = sweep_index+1;

                % calcualate cost, assuming tube is 1.5 m in length, and float is 5 cm tall
                tube_material = inputs.tube_mat;
                float_material = inputs.float_mat;
                tube_cost = tube_material.cost*tube_material.rho * 1.5*pi*(inputs.R_tube(1)^2-inputs.R_tube(2)^2);
                float_cost = float_material.cost*float_material.rho * .05*pi*(inputs.R_float(1)^2-inputs.R_float(2)^2);
                cost(sweep_index) = tube_cost + float_cost;

                % calculate safety factor of operation
                safety_factor(sweep_index) = min([tube_material.T_melt/outputs.tube_temp_max,float_material.T_melt/outputs.float_temp_max]);

                % estimate run time
                T = toc;
                disp(['Elapsed time: ' sprintf('%.3f',T) ' sec']);
                fprintf(['Estimated time remaining: ' sprintf('%.3f',(sweep_N-sweep_index+1)*T) ' sec' '\n']);

                fprintf(['or ' sprintf('%.3f',(sweep_N-sweep_index+1)*T/60) ' minutes!' '\n\n']);
            end
        end
    end

end

float_mat_names=cell(0);
for(m=[key.float_mat])
    float_mat_names(end+1) = {m.name};
end
tube_mat_names=cell(0);
for(m=[key.tube_mat])
    tube_mat_names(end+1) = {m.name};
end

safety_factor = safety_factor(2:end)
cost = cost(2:end)

R_tube_outer = reshape([key.R_tube],2,[])
R_tube_outer = R_tube_outer(1,:)
R_float_outer = reshape([key.R_float],2,[])
R_float_outer = R_float_outer(1,:)

scatter3(R_tube_outer/in_to_m,R_floats_outer/in_to_m,steam_rate*60*60) % little correlation, same with safety factor
xlabel('tube outer radius [in]')
ylabel('float outer radius [in]')
zlabel('steam production [L/hr]')

scatter(categorical(float_mat_names(safety_factor>1.2)),safety_factor(safety_factor>1.2)) % float materials with admissible safety factors
scatter(categorical(float_mat_names),safety_factor) % all safety factors

scatter(categorical(tube_mat_names(safety_factor>1.2)),safety_factor(safety_factor>1.2)) % tube materials with admissible safety factors
scatter(categorical(tube_mat_names),safety_factor) % all safety factors

tube_cats = categorical(tube_mat_names)
float_cats = categorical(float_mat_names)


scatter3(tube_cats(safety_factor>1.2),float_cats(safety_factor>1.2),steam_rate(safety_factor>1.2)*3600)
zlabel('steam production [L/hr]')
xlabel('tube material')
ylabel('float material')

% minimum cost solution
% limit to valid
I = safety_factor>1.2;

tube_cats_valid = tube_cats(I); float_cats_valid = float_cats(I); steam_rate_valid = steam_rate(I); cost_valid = cost(I);
R_tube_valid = R_tube_outer(I); R_float_valid = R_floats_outer(I);

[min_cost,i_min] = min(cost_valid);

scatter3(R_float_valid/in_to_m,R_tube_valid/in_to_m,cost_valid)
title('valid tube and float configurations')
xlabel('float radius [in]'); ylabel('tube radius [in]'); zlabel('cost [$]');

% optimal solution:
% tube radius 1"
% float radius .175"
% tube material glass
% float material carbide

