%% Graph object
%   Holds connectivity of all nodes and elements

classdef Graph < handle
    properties (SetObservable)
        nodes = Node();
        edges = Edge();
    end

    methods
        function obj = Graph(nodes, edges)
            if(nargin == 2)
                obj.edges = edges;
                obj.nodes = nodes;
            else
                obj.edges = [];
                obj.nodes = [];
            end
        end

        function neighbors = neighbors(this,node)
            % return neighboring nodes based on edges in graph
            edges = this.edges;
            c = 1;
            neighbors  = Node(); neighbors(1) = [];
            for(edge=edges)
                ind = edge.includes(node);
                if(ind)
                    neighbors(c) = edge.nodes(2-ind+1); % get other node
                    c=c+1;
                end
            end
        end

        function index = find(this,node)
            % find index of node in graph
            index = -1;
            i = 1;
            while(index < 0)
                if(node == this.nodes(i))
                    index = i;
                else
                    i = i+1;
                end
            end
        end

        function [] = define_delta(this)
            % if node does not have valid deltax deltay data, update
            nodes = this.nodes;
            for(node=nodes)
                if(isnan(node.deltax) || isnan(node.deltay))
                    deltax_vec = []; deltay_vec = []; % reset delta averages
                    d = 1;
                    for(neighbor=this.neighbors(node))
                        % calculate deltax and deltay for self coefficient
                        if([1 0]*(node.P-neighbor.P)) % in x direction
                            deltax_vec(d) = abs([1 0]*(node.P-neighbor.P));
                            d = d+1;
                        elseif([0 1]*(node.P-neighbor.P)) % in y direction
                            deltay_vec(d) = abs([0 1]*(node.P-neighbor.P));
                            d = d+1;
                        end

                    end

                    deltax = node.deltax; deltay = node.deltay;
                    % if undefined dimension, take from average of distances to nodes and assign to node
                    if(isnan(deltax))
                        deltax = mean(deltax_vec); node.deltax = deltax;
                    elseif(isnan(deltay))
                        deltay = mean(deltay_vec); node.deltay = deltay;
                    end
                end
            end
        end

        function T_dist = update(this,T_dist,E)
            % update nodes with given temperature and energy distributions
            % nodes need to determine if phase change is occuring; if so, will return a different T and take note in E
            i = 1;
            for(node = this.nodes)
                T_dist(i) = node.update(T_dist(i),E(i));
                i = i+1;
            end
        end

        function fig = plot(this,fig)
            % plot nodes colored by their temperature in figure fig
            % if already plotting in figure, update instead
            nodes = this.nodes;
            N = numel(nodes);
            X = zeros(4,N); Y = X; C = zeros(N,1,3);
            T = zeros(1,N);
            % color interpolation values
            C_hot = [252,52,38]/255; % red = hot
            C_hot = [255,40,33]/255; % red = hot
            C_cool = [12,223,235]/255; % blue = cold
            C_mid = [235,235,106]/255; % yellow = mid
            T_hot = 600; % C
            T_cool = 50; % C
            T_mid = 150; 
            % T_mid = mean([T_hot,T_cool]); % midpoint

            f = 1;
            while(f <= numel(fig.Children) && ~strcmp(fig.Children(f).Type,'axes'))
                f = f+1;
            end
            i = 1;
            if(isempty(fig.Children) || f > numel(fig.Children)) % no axes in fig
                % get plot values
                for(node = nodes)
                    % get node coordinates
                    % each node is a set of 4 coordinates
                    P = node.P;
                    deltax = node.deltax;
                    deltay = node.deltay;
                    coords = .5*[deltax 0;0 deltay]*[-1 1 1 -1; 1 1 -1 -1] + P; % coordinates of vertices
                    X(:,i) = coords(1,:)';
                    Y(:,i) = coords(2,:)';
                    % get node temperature
                    T(i) = node.T;
                    % convert temperature to color
                    % if temp is too low, choose min
                    if(T(i) < T_cool); C_node = C_cool;
                    else
                        C_node = interp1([T_cool T_mid T_hot],[C_cool;C_mid;C_hot],T(i));
                        if((isnan(C_node)))
                            C_node = C_hot;
                        end
                    end
                    C(i,1,:) = C_node;
                    i = i+1;
                end
                % create new axes
                ax = axes(fig);
                % create new patch
                patch_plot = patch(X,Y,C);

                % create new colormap
                Cmap = interp1([T_cool T_mid T_hot],[C_cool;C_mid;C_hot],T_cool:10:T_hot);
                colormap(Cmap);
                color_plot = colorbar; 
                caxis([T_cool T_hot]);

            else % get axes
                ax = fig.Children(f);
                % get updated temps
                for(node=nodes)
                    T(i) = node.T;
                    % convert temperature to color
                    % if temp is too low, choose min
                    if(T(i) < T_cool); C_node = C_cool;
                    else
                        C_node = interp1([T_cool T_mid T_hot],[C_cool;C_mid;C_hot],T(i));
                        if((isnan(C_node)))
                            C_node = C_hot;
                        end
                    end
                    C(i,1,:) = C_node;
                    i = i+1;
                end
                % get patch in axes
                patch_plot = ax.Children;
                % update patch colors
                patch_plot.CData = C;
            end
        end

        function [nodes_sel,i,r] = nodesNear(this,P_sel,R,R_min)
        % return nodes selected, indices of nodes selectd, and distance from point to node
            if(~exist('R_min','var'))
                R_min = 0;
            end
            % return all nodes that are within a radius R from a point P_sel
            nodes_sel = Node(); nodes_sel(1) = []; r = []; i = [];
            for(n = 1:numel(this.nodes))
                node = this.nodes(n);
                R_node = norm(node.P - P_sel);
                if((R_node - R) <= R*1e-5+10*eps && (R_node - R_min) >= 0)
                    nodes_sel(end+1) = node;
                    r(end+1) = R_node;
                    i(end+1) = n;
                end
            end
        end
    end

    methods(Static) % can be run without an instance 

        function graph = generate(grid_mat, grid_temp,key,step)
            % populate a graph based on a grid, key of materials, and step size
            % grid_mat is a matrix, where each entry is a number corresponding to an index of a material in key
            % grid_temp is a matrix of matching size where each entry is the initial temperature of the node
            % step is the corresponding distance between two nodes
            % assume that origin is at top left node (lazy), so have positive x and negative y
            % TODO: include convection field and radiation field
            
            % pre allocate
            [rows,cols] = size(grid_mat);
            nodes = Node();
            nodes = repelem(nodes,rows,cols);
            edges = Edge();
            numedges = 2*(rows-1)*(cols-1)+(rows-1)+(cols-1); % see notes
            edges = repelem(edges,numedges);
            e = 1; % edge index

            for(r = 1:rows)
                for(c = 1:cols)
                    mat = key(grid_mat(r,c));
                    T = grid_temp(r,c);
                    P = step*[c-1;-(r-1)];
                    E = 0; % placeholder
                    % deltax and deltay are assumed to be equal to step in general 2D
                    nodes(r,c) = Node(P,T,E,mat, step,step);
                    % edge creation
                    % create a node to the node immediately to the left and up
                    if(r~=1)
                        edges(e) = Edge([nodes(r,c),nodes(r-1,c)]); % create an edge to the left
                        e = e+1;
                    end
                    if(c~=1)
                        edges(e) = Edge([nodes(r,c),nodes(r,c-1)]); % create an edge to the up
                        e = e+1;
                    end
                end
            end

            % rearrange nodes to be a row vector, along rows then columns
            nodes = reshape(nodes',1,[]);
            graph = Graph(nodes,edges);
        

        end

    end
end
