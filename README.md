# ME 366J - Reverse Osmosis Joneses - Simulation

## Solar still subassembly simulation 

Finite element method transient conduction from concentrated solar radiation to water

2D simulation, implicit method solved with matrix inversion

Discrete time and space

Adapted From: _Fundamentals of Heat and Mass Transfer_. Incropera, Dewitt, Bergman, & Lavine
                PP. 302-317, 212-234

## Todo:
* Danial - ~~update: node.update function to include effects of boiling ~~
* Aaron - ~~create: graph.generate function to populate graph with a set of nodes and edges described by a grid~~
* Aaron - ~~create: graph.plot function to plot the nodes (and edges?) in a graph based on their shape, position, and temperature~~
* Aaron - ~~update: environment.step increase efficiency by limiting calls to temp_coefficients; only needs to be updated when node parameters are updated~~
* Danial- ~~ method of logging steam produced by nodes
* create: node absolute energy calculation, so that when energy is initialized energy starts as well~~
* Adrian - ~~research: material properties for specific materials~~
* Aaron -  ~~grids for specific situation~~
* Aaron -  ~~method for plotting results~~
* Aaron - ~~create: graph.position function to get node that is closest to given coordinate~~
* Aaron - create: method for parametric analysis and optimization
* research: material melting points of each potential material
* research: material costs 
* run: optimization of tube dimension
* run: optimization of float dimension
* run: optimization of focusing method
* run: optimization of tube material
* run: optimization of float material

## Optimization:

Results of Optimization:

Optimal solution within bounds:
tube radius: 1" x .75"
float radius: .175"
tube material: glass
float material: carbide

Output function is the steady state rate of steam production.

Cost function is the total monetary cost of the U-tube subsystem materials.

Optimization is constrained by the melting safety point of the materials used. If the simulation has a minimum safety factor of less than 1.25, the solar still is considered too dangerous for use.

Optimization parameters are the tube radius dimensions, float radius dimensions, tube, and float materials. 


## Documentation:

### Structure

Combination of object oriented programming and scripting

Written in Matlab

### Implementation

Boiling:
* Keep track of stored energy and temperature of node
* Once node reaches vaporization temperature, increase in energy results in an increase in stored energy, but not temperature
* When stored energy reaches energy of vaporization temp + enthalpy of vaporization, considered "boiled"
* ~~Once boiled, replace energy with lowest level of energy of neighbors~~ when boiling, keep track of energy lost due to boiling, to quantify steam production. Node should keep its temperature but 'lose energy' from steam so that system reaches a steady state

#### Objects

Material
* Properties: 
    * Name                    (name)
    * Thermal Conductivity    (k:W/mK)
    * Specific Heat Capacity  (c_p:J/kgK)
    * density                 (rho:kg/m^3)
    * thermal diffusivity     (alpha:m^2/s)
    * Vaporization Temp       (T_vap:C)
    * Enthalpy of Vapor.      (E_vap:J)

Node
* Properties: 
    * Temperature             (T:C)
    * Stored Energy           (E:J)
    * Position                (P:2x1 vect :m)
    * Material                (obj)
* Functions:
    * () = update(T_new,graph)
        * Update node temperature and energy based on material properties
        * Implementation of boiling process as above

Edge
* Properties: 
    * Nodes                   (nodes:2x1 vect Node)

Graph
* Properties: 
    * Nodes                   (nodes:collection of all nodes)
    * Edges                   (edges:connections between all nodes)
* Functions:
    * TODO: plot(fig)
        * plot temperature distribution of nodes in fig

Environment
* Properties:
    * Graph                   (graph: connected nodes)
    * Graph History           (graph_hist: 1xT vect)
* Functions:
    * TODO: 
    * TODO: (graph_new) = step(t_h)
        * perform time step march with time step t_h
        * create new graph with new nodes
        * update environment graph with new graph
        * store history of graph
