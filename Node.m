%% Node object
%   Represents discretized 2D space control volume
%   Nodes are updated each time step of the simulation

classdef Node < handle
    properties (SetObservable, AbortSet=true)      % properties that each node is
        P           % position, 2x1 (x,y)' coordinates: m
        deltax = nan; % width in x direction: m
        deltay = nan; % width in y direction: m
        I = 0;      % radiation energy intensity; default to no radiation
        h = 0;      % convection coefficient: W/m^2K; default to no convection
        material    % material object
        thick = .005; % thickness of 2D element
        Ah = 0;     % convection area; default to no convection
        AI = 0;     % irradiation area; default to no radiation
    end
    properties(Hidden, AbortSet=true)
        T           % temperature: deg_C
        E           % stored energy: J
        Tinf = 0;   % convection temperature: C; default to no convection
        Steamprod = 0; % Mass of steam produced, initial value = 0
    end

    methods         % every function that each node has
                    % constructor, MatLab will call this code to build a node
                    % TODO: deprecate form with energy input and call function of E(T)
        function obj = Node(P, T, E,Mat, deltax, deltay)
            if(nargin >=4)
                if(nargin <5)
                    deltay = nan;
                end
                if(nargin <6)
                    deltax = nan;
                end
                obj.deltax = deltax;
                obj.deltay = deltay;
                obj.P = P;
                obj.T = T;
                obj.E = E;
                obj.material = Mat;
            end
        end

        function [Tnew] = update(this,Tnew,Enew,h) 
            % update function to update temperature and energy based on simulation
            T_vap = this.material.T_vap;
            E_vap = this.material.E_vap;
            if(~isnan(T_vap) && ~isnan(E_vap))
                if( Tnew <= T_vap)
                    this.T = Tnew;
                    this.E = this.E + Enew;
                else
                    this.T = T_vap;
                    this.Steamprod = Enew/E_vap + this.Steamprod; % mass of steam produced
                    this.E = this.E + Enew;
                end
            else
                this.T = Tnew;
                this.E = this.E + Enew;
            end
        end
    end

    methods (Static)
        function handlePropEvents(src,evnt)
            disp([src.Name ' has changed']);
        end
    end

end
