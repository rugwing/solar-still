% script to run parametric optimization of simulation

% current inputs: 
%   float material
%   tube material
%   radii of tube
%   radii of float

sim_properties
in_to_m = .0254;
% example run
inputs = struct('float_mat',aluminum,'tube_mat',plex,'R_tube',[1 .75]*in_to_m,'R_float',[.5 0]*in_to_m);

float_mats = [aluminum, steel, nichrome, tungsten, brass, magnesium, carbide, styrofoam,glass];
tube_mats = [plex, polycarb, pyrex, glass];

% parameter name
param_name = 'R_tube';
% parameter values to sweep through; sweep through columns if a matrix
% example: vary tube diameter in sizes of pvc tube available
param_sweep = [1,      .75;
                1.315,  1;
                1.6,    1.25;
                1.9,    1.5;
                2.375,  2;
                2.875,  2.5;
                3.5,    3;]*in_to_m;

% output value storage in column vectors matching sweep
sweep_N = size(param_sweep,1);
steam_rate = zeros(1,sweep_N); 
sweep_index = 1;
float_temp_max = steam_rate; 
tube_temp_max = steam_rate;
cost = steam_rate;
safety_factor = steam_rate;
for(param=param_sweep')
    tic;

    % set parameter
    disp(['setting ' param_name ' to ']);
    details(param);
    inputs.(param_name) = param;

    % run simulation
    disp('running simulation');
    outputs = sim_parameterization(inputs);

    disp('storing outputs');
    % store outputs
    steam_rate(sweep_index) = outputs.steam_rate;
    float_temp_max(sweep_index) = outputs.float_temp_max;
    tube_temp_max(sweep_index) = outputs.tube_temp_max;
    sweep_index = sweep_index+1;

    % calcualate cost, assuming tube is 1.5 m in length, and float is 5 cm tall
    tube_material = inputs.tube_mat;
    float_material = inputs.float_mat;
    tube_cost = tube_material.cost*tube_material.rho * 1.5*pi*(inputs.R_tube(1)^2-inputs.R_tube(2)^2);
    float_cost = float_material.cost*float_material.rho * .05*pi*(inputs.R_float(1)^2-inputs.R_float(2)^2);
    cost(sweep_index) = tube_cost + float_cost;

    % calculate safety factor of operation
    safety_factor(sweep_index) = min([tube_material.T_melt/outputs.tube_temp_max,float_material.T_melt/outputs.float_temp_max]);

    % estimate run time
    T = toc;
    disp(['Elapsed time: ' sprintf('%.3f',T) ' sec']);
    fprintf(['Estimated time remaining: ' sprintf('%.3f',(sweep_N-sweep_index+1)*T) ' sec' '\n\n']);
end

% plot results
fig_steam_prod = figure(1); hold on; grid on;
title(['steam production vs ' strrep(param_name,'_',' ')]);
plot(param_sweep(:,1),steam_rate,'LineWidth',2);

fig_float = figure(2); hold on; grid on;
title(['float temp vs ' strrep(param_name,'_',' ')]);
plot(param_sweep(:,1),float_temp_max,'LineWidth',2);

fig_float = figure(3); hold on; grid on;
title(['tube temp vs ' strrep(param_name,'_',' ')]);
plot(param_sweep(:,1),tube_temp_max,'LineWidth',2);
