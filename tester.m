%% testing script file to run
clear;

aluminum = Material('aluminum',205,921.1,2700);
aluminum.alpha_s = .5;

steel = Material('steel',10,1000,34000); % made up
plex = Material('plex',10,1000,10000); % made up
water = Material('water',100,1000,1000); % made up
water.alpha_s = .2;

% populate graph
%
n =20;
L = 2*.0254; % 2 in to m
% L = 20; % 2 in to m
grid_mat = ones(n);
% grid_mat = [1 1 1; 1 1 1; 1 1 1];
grid_temp = 30*ones(n); % uniform temperature
key = [aluminum,steel];
step = L/n;
graph = Graph.generate(grid_mat,grid_temp,key,step);
fig_graph = figure();
graph.plot(fig_graph);

nodes_ring = graph.nodesNear([L/2;-L/2],L/2,L/2-2*L/n);
for(node=nodes_ring)
    node.Ah = (L/n)^2; node.h = 4000; node.Tinf = 20;
end

nodes_center = graph.nodesNear([L/2;-L/2],L/8);
for(node=nodes_center)
    % node.AI = 10*(L/n)^2; 
    node.AI = 1/13;
    node.I = 300;
end

% thickness is the thickness of the 2D elements being analyzed
ti = 0; tf = 8e3; dt = 200;
ti = 0; tf = 100; dt = 2;

env = Environment(graph,ti,tf,dt);
for(t=ti:dt:tf-dt)
    if(~mod(t,10*dt))
        disp(['time: ' num2str(t) ' / ' num2str(tf) ' sec']);
        graph.plot(fig_graph);
        pause(.05);
    end
    env.step;
end

% makeshift plot results
fig_temp_hist = figure();
plot(env.t_hist,env.T_hist','LineWidth',2);
% legend
labels = cell(1,numel(graph.nodes));
for(i = 1:numel(graph.nodes))
    labels(i) = {['Node ' num2str(i)]};
end
% legend(labels, 'Location','Best');

