%% Environment object
%   Represents connection between two nodes

classdef Environment < handle
    properties
        graph = Graph();       % connected nodes
        T_hist;                 % time history of Nxt temperature distribution, where N is the number of nodes, t is the number of time steps
        steam_hist;             
        t_cur;                  % current time value
        dt;                  % time step
        t_hist;                 % time history of 1xt time steps, where t = numel(t_hist): t_hist = ti:dt:tf;
        t_i;                    % time index
        % T_hist wil be more efficient than storing graphs for large time histories
        % will need a graph function that 

        % graph_hist = Graph();    % vector of time history not including current graph of nodes stored in graph
        coef_mat;           % matrix of coefficients; updated when nodes change parameters, otherwise obtained from this variable
    end

    methods
        function obj = Environment(graph,ti,tf,dt)
            if(nargin <1)
                graph = [];
            else
                obj.graph = graph;
                % create listener for the nodes in graph
                for(node=graph.nodes)
                    node.addlistener(properties(Node),'PostSet',@obj.update_coef);
                end
                graph.addlistener('edges','PostSet',@obj.update_coef);

                % define histories and time
                obj.reset(ti:dt:tf);
                % define coefficient matrix
                obj.update_coef();
            end
        end

        function [coef_mat] = update_coef(this,src,evnt)
            disp('updating matrix');
            this.coef_mat = this.temp_coefficients(this.dt);
            % keyboard
        end

        function [] = reset(this,t_vec);
            % function that resets histories and assigns initial conditions according to the graph
            this.t_cur = t_vec(1);
            this.dt    = t_vec(2)-t_vec(1);
            this.t_hist = t_vec;
            i = 1;
            T_hist = zeros(numel(this.graph.nodes),numel(t_vec));
            for(node=this.graph.nodes)
                T_hist(i,1) = node.T;
                i = i+1;
            end
            this.T_hist = T_hist;
        end

        function [] = step(this,dt)
            if(~exist('dt''var'))
                dt = this.dt;
            end
            % Process:
            %   For each node, find connected nodes
            %   for each connection, calculate heat transfer 
            %
            % update deltax deltay
            this.graph.define_delta();

            % get coefficient matrix
            coef_mat = this.coef_mat;
            % generate rhs of equations
            rhs = this.temp_rhs(dt);
            % invert to find temperature distribution
            T_next = coef_mat \ rhs; % matlab love

            % calculate energy transfer to each node
            E = zeros(numel(this.graph.nodes),1); % Energy vector
            for(i = 1:numel(this.graph.nodes))
                node = this.graph.nodes(i);
                mat = node.material;
                T = node.T;
                E(i) = mat.rho*mat.c_p*node.deltax*node.deltay*node.thick*(T_next(i)-T);
            end
            % accuracy of E about 10^-10

            % update node temperatures with temperatures and energy transfer
            T_next = this.graph.update(T_next,E);
            % store temperature history
            % TODO: replace with index
            this.T_hist(:,abs(this.t_hist-(this.t_cur+dt))<1e-10) = T_next;
            % step forward in time
            this.t_cur = this.t_cur + dt;
            % keyboard
            % update temperatures and energies in nodes 
        end

        function coef_mat = temp_coefficients(this,dt)
            if(~exist('dt','var'))
                dt = this.dt;
            end
            % calculate coefficient matrix of future temperatures
            nodes = this.graph.nodes;
            graph = this.graph;

            % allocate coeff_mat
            coef_mat = zeros(numel(nodes));
            % create equation for each node
            for(node=nodes)
                index_node = graph.find(node);
                rho = node.material.rho; c = node.material.c_p; k = node.material.k;
                deltax = node.deltax; deltay = node.deltay; t = node.thick;

                coef_self = 0; % self coefficient depends on which neighbors
                % gather coefficients of neighbors
                for(neighbor=graph.neighbors(node))
                    % index of neighbor in matrix
                    index_neighbor = graph.find(neighbor);
                    % determine if neighbor in x or y direction
                    dir = neighbor.P-node.P;
                    % account for differing thickness between objects - avg
                    t_avg = (t + neighbor.thick)/2;
                    if(abs(dir(2)) > abs(dir(1))) % in y direction
                        A = t_avg*deltax;
                    else
                        A = t_avg*deltay;
                    end
                    % account for differing k between objects - assume average of k between
                    k_avg = (k + neighbor.material.k)/2;
                    coef = A*k_avg/norm(dir);
                    % store coefficient in matrix
                    coef_mat(index_node,index_neighbor) = coef;

                    % add to self coefficient
                    coef_self = coef_self - coef;
                end

                % calculate coefficient of self
                coef_mat(index_node,index_node) = coef_self + - node.h*node.Ah ... % total convection
                    - rho*c*deltax*deltay*node.thick/dt; % transient
            end

        end

        function rhs_vec = temp_rhs(this,dt)
            if(~exist('dt','var'))
                dt = this.dt;
            end
            % calculate right hand side vector of knowns
            nodes = this.graph.nodes;
            graph = this.graph;

            % allocate coeff_mat
            rhs_vec = zeros(numel(nodes),1);
            % create rhs for each node
            for(node=nodes)
                index_node = graph.find(node);
                deltax = node.deltax; deltay = node.deltay;
                rho = node.material.rho; c = node.material.c_p;

                rhs_vec(index_node) = -node.I*node.AI*node.material.alpha_s ... % incoming radiation energy
                    - node.h*node.Ah*node.Tinf ... % convection
                    - rho*c*deltax*deltay*node.thick*node.T/dt; % transient
            end
        end
    end
    
end
