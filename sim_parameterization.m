% analysis function that takes in certain parameters and outputs desired outputs
% inputs is a struct, outputs is a struct
function [outputs] = sim_parameterization(inputs)
    plot_flag = false;
    % current inputs:
    float_mat = inputs.float_mat;
    tube_mat  = inputs.tube_mat;
    R_tube = inputs.R_tube;
    R_float = inputs.R_float;
    % focus_method = inputs.focus_method; out of scope

    % clear; clc;
    in_to_m = .0254; % conversion from inches to m
    % Material Properties

    sim_properties; % generate material properties
    % parameterized
    % key = [water,aluminum,plex,air];
    key = [water,float_mat,tube_mat,air];
    key(2).alpha_s = .9; % paint black
    key(3).alpha_s = .1; % change for cloudiness
    key(4).alpha_s = .1; % clarity of water

    % Environment Properties
    T_inf = 30;
    h_air = 2;
    I_sun = sunny; % solar irradiance
    % I_sun = cloudy; % cloudy irradiance

    % Physical Properties
    % replaced as inputs ~~
    % R_tube = [1 .85]*in_to_m;
    % R_float = [.5 0]*in_to_m;
    % ~~
    A_mirror = 1;

    n =20;
    n = 15;
    L = R_tube(1)*2;

    grid_mat = 4*ones(n);
    grid_temp = T_inf*ones(n); % uniform temperature
    step = L/(n-1);
    graph = Graph.generate(grid_mat,grid_temp,key,step);

    [nodes_tube,i_tube] = graph.nodesNear([L/2;-L/2],R_tube(1),R_tube(2));
    disp(['tube nodes: ' num2str(numel(nodes_tube))]);
    for(node=nodes_tube)
        % node.Ah = (step)^2; node.h = h_air; node.Tinf = T_inf;
        node.material = key(3);
    end
    [nodes_tube_outside,i_tube_outside] = graph.nodesNear([L/2;-L/2],R_tube(1),R_tube(1)-step);
    % disp(['tube outside nodes: ' num2str(numel(nodes_tube_outside))]);
    for(node=nodes_tube_outside)
        node.Ah = (step)^2; node.h = h_air; node.Tinf = T_inf;
    end

    [nodes_water,i_water] = graph.nodesNear([L/2;-L/2],R_tube(2),R_float(1));
    disp(['water nodes: ' num2str(numel(nodes_water))]);
    for(node = nodes_water)
        node.material = key(1);
    end

    [nodes_float,i_float] = graph.nodesNear([L/2;-L/2],R_float(1),R_float(2));
    disp(['float nodes: ' num2str(numel(nodes_float))]);
    for(node=nodes_float)
        node.material = key(2);
    end

    % TODO: replace with selection of focus_method
    %
    % nodes_float_rad = graph.nodesNear([L/2;-L/2],R_float(1),R_float(1)-step); % exterior of float
    % % get points on left half of float
    % nodes_float_rad_temp = nodes_float_rad; nodes_float_rad = Node(); nodes_float_rad(1) = [];
    % for(node=nodes_float_rad_temp)
    %     if(node.P(1) <= L/2) % select left half of nodes
    %         nodes_float_rad(end+1) = node;
    %     end
    % end
    % keyboard
    [nodes_float_rad,i_float_rad] = graph.nodesNear([L/2-R_float(1)+step;-L/2],step); % left side point on float

    disp(['radiation nodes: ' num2str(numel(nodes_float_rad))]);
    for(node=nodes_float_rad)
        % node.AI = 10*(L/n)^2; 
        node.AI = A_mirror/numel(nodes_float_rad); % uniformly focused over all radiated
        node.I = I_sun;
        node.material = key(2);
    end

    if(plot_flag)
        fig_graph = figure();
        graph.plot(fig_graph);
    end

    % 1 hour solver
    % ti = 0; tf = 60*60; dt = 20; % run for 1 hour, time step of 30 sec
    % 5 min solver
    ti = 0; tf = 5*60; dt = 2; % run for 5 min, time step of 2 sec
    ti = 0; tf = 2.5*60; dt = 5; % run for 2.5 min, time step of 5 sec


    T = ti:dt:tf-dt;
    T_avg_float = zeros(1,numel(T));
    T_avg_float_rad = T_avg_float;
    T_avg_water = T_avg_float;
    T_avg_tube = T_avg_float;
    steam_prod = T_avg_float;
    i = 1;
    env = Environment(graph,ti,tf,dt);
    for(t=T)
        if(~mod(t+1,5*dt))
            disp(['time: ' num2str(t) ' / ' num2str(tf) ' sec']);
            if(plot_flag)
                graph.plot(fig_graph);
                pause(.005);
            end
        end
        % store values
        Tsum = 0;
        for(n=nodes_tube)
            Tsum = Tsum + n.T;
        end
        T_avg_tube(i) = Tsum/numel(nodes_tube);
        Tsum = 0;
        for(n=nodes_float)
            Tsum = Tsum + n.T;
        end
        T_avg_float(i) = Tsum/numel(nodes_float);
        Tsum = 0;
        for(n=nodes_float_rad)
            Tsum = Tsum + n.T;
        end
        T_avg_float_rad(i) = Tsum/numel(nodes_float_rad);
        Tsum = 0;
        for(n=nodes_water)
            Tsum = Tsum + n.T;
        end
        T_avg_water(i) = Tsum/numel(nodes_water);

        for(n=graph.nodes)
            steam_prod(i) = steam_prod(i)+n.Steamprod;
        end

        i = i+1;

        env.step;
    end

    if(plot_flag)
        % makeshift plot results
        fig_temp_hist = figure();
        plot(env.t_hist,env.T_hist','LineWidth',2);
        % legend
        labels = cell(1,numel(graph.nodes));
        for(i = 1:numel(graph.nodes))
            labels(i) = {['Node ' num2str(i)]};
        end

        fig_temp_ags = figure();
        hold on;
        plot(T,T_avg_float_rad,'DisplayName','float rad temp avg','LineWidth',2);
        plot(T,T_avg_float,'DisplayName','float temp avg','LineWidth',2);
        plot(T,T_avg_tube,'DisplayName','tube temp avg','LineWidth',2);
        plot(T,T_avg_water,'DisplayName','water temp avg','LineWidth',2);
        legend show;
        fig_steam = figure();
        hold on;
        plot(T,steam_prod,'DisplayName','steam production','LineWidth',2);

        % legend(labels, 'Location','Best');

        figure(fig_graph);
        rectangle('Position',[L/2-R_tube(1) -L/2-R_tube(1) R_tube(1)*2 R_tube(1)*2],'Curvature',[1 1])
        rectangle('Position',[L/2-R_tube(2) -L/2-R_tube(2) R_tube(2)*2 R_tube(2)*2],'Curvature',[1 1])
        rectangle('Position',[L/2-R_float(1) -L/2-R_float(1) R_float(1)*2 R_float(1)*2],'Curvature',[1 1])

    end
    % outputs: steam rate, Float temperature max, Tube temperature max, 
    % calculate steam rate by central difference
    steam_rate = (steam_prod(end)-steam_prod(end-2))/(2*dt);
    float_temp_max = max(max(env.T_hist(i_float,:)));
    tube_temp_max = max(max(env.T_hist(i_tube,:)));
    outputs = struct('steam_rate',steam_rate,'float_temp_max',float_temp_max,'tube_temp_max',tube_temp_max);
end
