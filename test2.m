%% testing script for efficiency
clear;

aluminum = Material('aluminum',205,921.1,2700);
aluminum.alpha_s = .5;

steel = Material('steel',10,1000,100); % made up

% condition must be met: spacing between nodes has to equal the width and height of each node
% edges = e12;

% populate graph
n =5;
grid_mat = ones(n);
% grid_mat = [1 1 1; 1 1 1; 1 1 1];
grid_temp = 100*ones(n); % uniform temperature
key = [aluminum,steel];
step = 1;
graph = Graph.generate(grid_mat,grid_temp,key,step);
fig_graph = figure();
graph.plot(fig_graph);

n_conv = graph.nodes(1); % node with convection
n_conv.h = 10; n_conv.Ah = 1; n_conv.Tinf = 0;
n_rad = graph.nodes(n^2); % node with radiation
n_rad.AI = 1; n_rad.I = 250; 

% thickness is the thickness of the 2D elements being analyzed
ti = 0; tf = 1e5; dt = 5*1e1;
env = Environment(graph,ti,tf,dt);
for(t=ti:dt:tf-dt)
    if(~mod(t,100*dt))
        disp(['time: ' num2str(t) ' / ' num2str(tf) ' sec']);
        graph.plot(fig_graph);
        pause(.05);
    end
    env.step;
end

% makeshift plot results
fig_temp_hist = figure();
plot(env.t_hist,env.T_hist','LineWidth',2);
% legend
labels = cell(1,numel(graph.nodes));
for(i = 1:numel(graph.nodes))
    labels(i) = {['Node ' num2str(i)]};
end
% legend(labels, 'Location','Best');

